<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'dashboard'], function() {
    Auth::routes([
        'register' => false,
        'reset' => false,
        'verify' => false,
    ]);
});

Route::get('/', 'Virtual360Controller@index');

Route::group(['middleware' => ['auth'], 'prefix' => 'dashboard'], function () {

    Route::get('/', 'HomeController@index')->name('dashboard');

    Route::resource('categories', 'CategoriesController');

    Route::resource('books', 'BooksController');
    
    /**
     * Profile & User Route
     */
    Route::get('profile', 'UsersController@profile')->name('profile');
    Route::patch('profile/{user}', 'UsersController@profileUpdate')->name('profile.update');
    Route::resource('users', 'UsersController');

    /**
     * Roles & Permissions Administrator Route
     */
    Route::resource('roles', 'RolesController');
    Route::patch('permissions/sort-module', 'PermissionsController@sortModule')->name('permissions.sort-module');
    Route::resource('permissions', 'PermissionsController', ['except' => [
        'create', 'show', 'edit'
    ]]);
});
