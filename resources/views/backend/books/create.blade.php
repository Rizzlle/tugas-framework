@extends('layouts.backend.app')

@section('title', 'Dashboard')

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables.css') }}">
<link rel="stylesheet" href="{{ asset('modules/select2/css/select2.min.css') }}">
@endsection

@section('content')
<div class="section-header border-top">

    <div class="section-header-back">
        <a href="{{ route('books.index') }}" class="btn btn-icon">
            <i class="fas fa-arrow-left"></i>
        </a>
    </div>

    <h1>Tambah Kategori Buku</h1>

</div>

<div class="row">
    <div class="col-sm-6">

        <div class="card">

            <form method="POST" action="{{ route('books.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Ups!</strong> ada masalah pada saat membuat acara.
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>
                                        {{ $error }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="bgImage">Gambar Buku</label>
                        <input type="file" name="image" class="dropify" data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png" data-max-file-size-preview="1M" />
                        <small class="text-muted">Maksimal gambar yang bisa diupload : 1MB</small>
                    </div>
    
                    <div class="form-group">
                        <label for="name">Judul Buku</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}" placeholder="Contoh Buku upin upin..." required autofocus>
                    </div>

                    <div class="form-group">
                        <label for="category_id">Kategori Buku</label>
                        <select class="form-control select2" name="category_id" id="category_id" data-placeholder="Pilih Kategori Buku">
                            <option></option>
                            @foreach ($categories as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Harga Buku</label>
                                <input type="text" name="price" id="price" class="form-control" value="{{ old('price') }}" required autofocus>
                                <small class="text-muted">Contoh: 200,000</small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Stok Buku</label>
                                <input type="number" name="stock" id="stock" class="form-control" value="{{ old('name') }}" required autofocus>
                                <small class="text-muted">Masukkan angka saja</small>
                            </div>
                        </div>
                    </div>
    
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>    
            </form>

        </div>

    </div>
</div>
@endsection

@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script src="{{ asset('modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('modules/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('modules/select2/js/select2.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/cleave.js@1.6.0/dist/cleave.min.js"></script>
<script>

    $('#dataTable').DataTable()
    $('.dropify').dropify();

    var cleave = new Cleave('#price', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });

</script>
@endsection
