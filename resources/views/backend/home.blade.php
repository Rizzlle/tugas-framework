@extends('layouts.backend.app')

@section('title', 'Dashboard')

@section('styles')
<link rel="stylesheet" href="{{ asset('modules/fotorama/fotorama.css') }}">
<link rel="stylesheet" href="{{ asset('modules/select2/css/select2.min.css') }}">
@endsection

@section('content')
<div class="section-header border-top">
    <h1>Dashboard</h1>
</div>

<div class="section-body">
    <h2 class="section-title">Selamat datang di dashboard <span class="text-primary">Dashboard Tokoku</span></h2>
</div>

<div class="row">
    <div class="col-sm-12">

        <div class="card">
            <div class="card-body">
                
            </div>
        </div>

    </div>
</div>
@endsection

@section('javascript')

@endsection
