@extends('layouts.backend.app')

@section('title', 'Dashboard')

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables.css') }}">
@endsection

@section('content')
<div class="section-header border-top">

    <div class="section-header-back">
        <a href="{{ route('categories.index') }}" class="btn btn-icon">
            <i class="fas fa-arrow-left"></i>
        </a>
    </div>

    <h1>Tambah Kategori Buku</h1>

</div>

<div class="row">
    <div class="col-sm-6">

        <div class="card">

            <form method="POST" action="{{ route('categories.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Ups!</strong> ada masalah pada saat membuat acara.
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>
                                        {{ $error }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="bgImage">Gambar Kategori</label>
                        <input type="file" name="image" class="dropify" data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png" data-max-file-size-preview="1M" />
                        <small class="text-muted">Maksimal gambar yang bisa diupload : 1MB</small>
                    </div>
    
                    <div class="form-group">
                        <label for="name">Nama Kategori</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" required autofocus>
                    </div>
    
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>    
            </form>

        </div>

    </div>
</div>
@endsection

@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script src="{{ asset('modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('modules/sweetalert/sweetalert.min.js') }}"></script>
<script>

    $('#dataTable').DataTable()
    $('.dropify').dropify();

</script>
@endsection
