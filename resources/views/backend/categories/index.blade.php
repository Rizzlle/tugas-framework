@extends('layouts.backend.app')

@section('title', 'Dashboard')

@section('styles')
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables.css') }}">
@endsection

@section('content')
<div class="section-header border-top">
    <h1>Kategori Buku</h1>

    <div class="section-header-button ml-auto">
        <a href="{{ route('categories.create') }}" class="btn btn-primary">
            Tambah Kategori
        </a>
    </div>

</div>

<div class="row">
    <div class="col-sm-12">

        <div class="card">
            <div class="card-body">
                <table class="table table-striped data-table" id="dataTable">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Gambar Kategori</th>
                            <th>Nama Kategori</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($categories as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <img src="{{ asset('uploads/category_images/' . $item->image) }}" alt="{{ $item->image }}" width="80" height="80">
                                </td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    <div class="btn-group mr-2">
                                        <button type="submit" id="button-delete-{{ $item->id }}" class="btn btn-action btn-danger" data-route="{{ route('categories.destroy', $item->id) }}" onclick="delete_data({{ $item->id }})">Hapus</button>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-action btn-info" href="{{ route('categories.edit', $item->id) }}">Edit</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('modules/sweetalert/sweetalert.min.js') }}"></script>
<script>

    var tableData = $('#dataTable').DataTable()

    @if($message = Session::get('success'))
        toastr.success('{{ $message }}', 'Success')
    @endif

    function delete_data(id)
        {
            var formUrl = $('#button-delete-' + id).data('route');

            swal({
                title: 'Yakin?',
                text: 'Apakah kamu ingin menghapus data ini?',
                buttons: {
                    cancel: true,
                    confirm: {
                        text: 'Hapus!',
                        closeModal: false
                    }
                },
                dangerMode: true,
                closeOnClickOutside: false
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                        type: 'DELETE',
                        url: formUrl,
                        dataType: 'JSON',
                        data: {'id': id, '_token': '{{ csrf_token() }}'},
                        success: function(res)
                        {
                            swal.stopLoading()
                            swal.close()

                            if (res.status === true) {
                                toastr.success(res.message, 'Success')
                                location.reload()
                            }
                        },
                        error: function(jqXHR, textStatus, errorthrown)
                        {
                            console.log(jqXHR);
                            swal.stopLoading()
                            swal.close()
                            toastr.error('Gagal menghapus Data', 'Error')
                        }
                    })
                }
            })
        }



</script>
@endsection
