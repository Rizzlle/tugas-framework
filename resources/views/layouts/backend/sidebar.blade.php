<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('dashboard') }}">
                <img src="{{ asset('images/logo.png')}}" alt="logo" width="30" height="30">
                <span class="ml-1 text-primary">Tokoku</span>
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('dashboard') }}">
                <img src="{{ asset('images/logo.png')}}" width="30" height="30" alt="logo">
            </a>
        </div>
        <ul class="sidebar-menu">
            <li {!! request()->routeIs('dashboard') == true ? 'class="active"' : null !!}>
                <a class="nav-link" href="{{ route('dashboard') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a>
            </li>

            <li class="menu-header">Master Data</li>
            @can('categories-list')
            <li {!! request()->routeIs('categories*') == true ? 'class="active"' : null !!}>
                <a href="{{ route('categories.index') }}" class="nav-link"><i class="fas fa-tag"></i> <span>Kategori Buku</span></a>
            </li>
            @endcan
            
            @can('roles-list')
            <li {!! request()->routeIs('books*') == true ? 'class="active"' : null !!}>
                <a href="{{ route('books.index') }}" class="nav-link"><i class="fas fa-book"></i> <span>Buku</span></a>
            </li>
            @endcan
            
            <li class="menu-header">Pengaturan</li>
            @can('users-list')
            <li {!! request()->routeIs('users*') == true ? 'class="active"' : null !!}>
                <a href="{{ route('users.index') }}" class="nav-link"><i class="far fa-id-badge"></i> <span>Administrator</span></a>
            </li>
            @endcan
            
            @can('roles-list')
            <li {!! request()->routeIs('roles*') == true ? 'class="active"' : null !!}>
                <a href="{{ route('roles.index') }}" class="nav-link"><i class="fas fa-user-shield"></i> <span>Level Administrator</span></a>
            </li>
            @endcan
        </ul>
    </aside>
</div>