<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CategoriesController extends Controller
{

    private $pathFolder = 'uploads/category_images/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Category::orderBy('created_at', 'DESC')->get();

        return view('backend.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'string|required',
            'image'     => 'required|mimes:png,jpg,jpeg|max:1048'
        ],
        
        [
            'required'  => ':attribute harus diisi',
            'mimes'     => 'Gambar tidak sesuai format',
            'max'       => 'Maksimal ukuran gambar adalah 1Mb'
        ],
        [
            'name'      => 'Nama Kategori',
            'image'     => 'Gambar Kategori'
        ]);

        $image                = $request->file('image');
        $image_name           = $request->get('name') . '-kategori-' . Str::random(4) . '-' . time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path($this->pathFolder), $image_name);


        Category::create([
            'image'     => $image_name,
            'name'      => $request->get('name')
        ]);

        return redirect()->route('categories.index')->with('success', 'Kategori Buku berhasil ditambah');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $category = Category::find($id);

        return view('backend.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'string|required',
            'image'     => 'mimes:png,jpg,jpeg|max:1048'
        ],
        
        [
            'required'  => ':attribute harus diisi',
            'mimes'     => 'Gambar tidak sesuai format',
            'max'       => 'Maksimal ukuran gambar adalah 1Mb'
        ],
        [
            'name'      => 'Nama Kategori',
            'image'     => 'Gambar Kategori'
        ]);

        $category = Category::find($id);

        if ($request->hasFile('image')) {
            $image                = $request->file('image');
            $image_name           = $request->get('name') . '-kategori-' . Str::random(4) . '-' . time() . '.' . $image->getClientOriginalExtension();
            $request->image->move(public_path($this->pathFolder), $image_name);

            if (File::exists($this->pathFolder . $category)) {
                File::delete($this->pathFolder . $category);
            }
        } else {
            $image_name = $category->image;
        }

        $category->update([
            'image'     => $image_name,
            'name'      => $request->get('name')
        ]);        

        return redirect()->route('categories.index')->with('success', 'Kategori Buku berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        
        $category = Category::find($id);

        if (File::exists($this->pathFolder . $category->image)) {
            File::delete($this->pathFolder . $category->image);
        }

        $category->delete();

        return response()->json(['status' => true, 'message' => 'Kategori Buku Berhasil dihapus']);

    }
}
