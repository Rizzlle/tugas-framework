<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BooksController extends Controller
{

    private $pathFolder = 'uploads/book_images/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with(['category'])->orderBy('created_at', 'DESC')->get();

        return view('backend.books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->get();

        return view('backend.books.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'         => 'string|required',
            'image'         => 'required|mimes:png,jpg,jpeg|max:1048',
            'category_id'   => 'required',
            'price'         => 'required',
            'stock'         => 'required'
        ],
        
        [
            'required'  => ':attribute harus diisi',
            'mimes'     => 'Gambar tidak sesuai format',
            'max'       => 'Maksimal ukuran gambar adalah 1Mb'
        ],
        [
            'title'         => 'Judul Buku',
            'image'         => 'Gambar Buku',
            'category_id'   => 'Kategori Buku',
            'price'         => 'Harga Buku',
            'stock'         => 'Stok Buku'
        ]);

        $image                = $request->file('image');
        $image_name           = $request->get('title') . '-buku-' . Str::random(4) . '-' . time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path($this->pathFolder), $image_name);

        Book::create([
            'category_id'       => $request->get('category_id'),
            'title'             => $request->get('title'),
            'image'             => $image_name,
            'stock'             => $request->get('stock'),
            'price'             => str_replace(',', '', $request->get('price')),
        ]);

        return redirect()->route('books.index')->with('success', 'Buku berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $categories = Category::orderBy('name', 'ASC')->get();
        $book = Book::find($id);

        return view('backend.books.edit', compact('categories', 'book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'         => 'string|required',
            'image'         => 'mimes:png,jpg,jpeg|max:1048',
            'category_id'   => 'required',
            'price'         => 'required',
            'stock'         => 'required'
        ],
        
        [
            'required'  => ':attribute harus diisi',
            'mimes'     => 'Gambar tidak sesuai format',
            'max'       => 'Maksimal ukuran gambar adalah 1Mb'
        ],
        [
            'title'         => 'Judul Buku',
            'image'         => 'Gambar Buku',
            'category_id'   => 'Kategori Buku',
            'price'         => 'Harga Buku',
            'stock'         => 'Stok Buku'
        ]);

        $book = Book::find($id);

        if ($request->hasFile('image')) {
            $image                = $request->file('image');
            $image_name           = $request->get('title') . '-buku-' . Str::random(4) . '-' . time() . '.' . $image->getClientOriginalExtension();
            $request->image->move(public_path($this->pathFolder), $image_name);

            if (File::exists($this->pathFolder . $book->image)) {
                File::delete($this->pathFolder . $book->image);
            }
        } else {
            $image_name = $book->image;
        }

        $book->update([
            'category_id'       => $request->get('category_id'),
            'title'             => $request->get('title'),
            'image'             => $image_name,
            'stock'             => $request->get('stock'),
            'price'             => str_replace(',', '', $request->get('price')),
        ]);

        return redirect()->route('books.index')->with('success', 'Buku berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $book = Book::find($id);

        if (File::exists($this->pathFolder . $book->image)) {
            File::delete($this->pathFolder . $book->image);
        }

        $book->delete();

        return response()->json(['status' => true, 'message' => 'Buku Berhasil dihapus']);
    }
}
