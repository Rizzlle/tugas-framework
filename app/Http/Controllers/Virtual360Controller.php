<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Virtual360Controller extends Controller
{
    public function index(Request $request)
    {
        return view('frontend.index');
    }
}
