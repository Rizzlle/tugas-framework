<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'Books';
    protected $fillable = [
        'category_id',
        'title',
        'image',
        'stock',
        'price'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }
}
