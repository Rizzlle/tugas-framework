<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\PermissionsLabel;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        $permissions = [
            [
                'label' => 'Manajemen Level',
                'route' => 'roles',
                'routes' => [
                    'roles-list',
                    'roles-create',
                    'roles-edit',
                    'roles-delete'
                ]
            ],
            [
                'label' => 'Manajemen Administrator',
                'route' => 'users',
                'routes' => [
                    'users-list',
                    'users-create',
                    'users-edit',
                    'users-delete'
                ]
            ]
        ];

        foreach ($permissions as $index => $permission) {
            $label = PermissionsLabel::create([
                                            'label' => $permission['label'], 
                                            'route' => $permission['route'], 
                                            'position' => $index
                                    ]);
            foreach ($permission['routes'] as $route) {
                Permission::create(['label_id' => $label->id, 'name' => $route]);
            }
        }

        $role = Role::create(['name' => 'superadmin', 'fixed' => true]);
        $role->givePermissionTo(Permission::all());

        /**
         * Permission Superadmin to 1
         */
        DB::table('model_has_roles')->insert([
            'role_id'       => 1,
            'model_type'    => 'App\User',
            'model_id'      => 1
        ]);
    }
}
